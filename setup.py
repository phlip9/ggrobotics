from setuptools import setup

setup(name='ggrobotics', version='1.0',
      description='Golden Gears Robotics site.',
      author='Philip Hayes', author_email='philiphayes9@gmail.com',
      url='http://ggrobotics.com/',

      #  Uncomment one or more lines below in the install_requires section
      #  for the specific client drivers/modules your application needs.
      install_requires=['Flask'],
     )
