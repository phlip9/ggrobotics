from flask import Flask

app = Flask(__name__)

from ggrobotics import views

app.config.from_pyfile('ggrobotics.cfg')
