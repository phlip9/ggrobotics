from flask import render_template
from ggrobotics import app

@app.route('/')
def index():
    """
    Renders the site index.
    """
    return render_template('index.html')
