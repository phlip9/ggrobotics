GGRobotics Team Website
=======================
Team website for Golden Gears Robotics (Team 4413).

Live website: [ggrobotics.com](http://www.ggrobotics.com/)


## Preparing the build environment: ##

* Install Python 3

        sudo apt-get install python3

* Install pip (for Python 3)

        sudo apt-get install python3-pip

* Install virtualenv

        sudo pip install virtualenv

* Install and setup virtualenvwrapper (optional but useful)

        sudo pip install virtualenvwrapper
        export WORKON_ENV=~/.virtualenvs
        mkdir -p $WORKON_ENV
        export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
        source /usr/local/bin/virtualenvwrapper.sh

* Make a virtualenv ('ggrobotics' is fine)

        mkvirtualenv ggrobotics

* Activate the virtualenv

        workon ggrobotics

* Install the Python dependencies

        pip install requirements.txt

* Install npm

        sudo apt-get install npm

* Install the npm dependencies

        sudo npm install -g recess


## Dependencies: ##

* pip install
    Flask
    Flask-Assets
* npm install -g
    recess


## Developing ##
Remember, this repository is version controlled.
Make commits often.
Work in separate branches when working on new features.
Make sure to pull in new changes before you start working.


## Debugging ##
Debugging involves running the lightweight webserver included with Flask.

### Running the webserver: ###

    # Make sure you've activated the ggrobotics virtualenv.
    workon ggrobotics
    # Run the webserver.
    python wsgi/run.py
    # The debugging webserver should be running on localhost:5000
    # just point your web browser to the url.


## Deploy ##
Get me (Philip) to add your ssh public key to the OpenShift webserver.

        git push deploy master

